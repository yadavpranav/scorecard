package com.student.grades;

/******
 * 
 * @author PRANAV YADAV
 *
 */

public class scorecard {
	private String Physics;
	private String Chemistry;
	private String Math;
	private int Total;
	private char Grade;

	public scorecard() {

	}

	public String getPhysics() {
		return Physics;
	}

	public void setPhysics(String Physics) {
		this.Physics = Physics;
	}

	public String getChemistry() {
		return Chemistry;
	}

	public void setChemistry(String Chemistry) {
		this.Chemistry = Chemistry;
	}

	public String getMath() {
		return Math;
	}

	public void setMath(String Math) {
		this.Math = Math;
	}

	public String getTotal() {
		return Integer.toString(Total);
	}

	public void setTotal(int Total) {
		this.Total = Total;
	}

	public char getGrade() {
		return Grade;
	}

	public void setGrade(char Grade) {
		this.Grade = Grade;
	}

}
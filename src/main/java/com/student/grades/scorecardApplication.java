package com.student.grades;

/******
 * 
 * @author PRANAV YADAV
 * 
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class scorecardApplication {

	public static void main(String[] args) {

		SpringApplication.run(scorecardApplication.class, args);

	}

}

package com.student.grades;
/******
 *
 *@author PRANAV YADAV
 *
 */

import java.lang.Exception;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class scorecardController {

	public int total(String p, String c, String m) {

		return (Integer.valueOf(p) + Integer.valueOf(c) + Integer.valueOf(m));
	}

	public char gd(String g) {
		if ((Integer.valueOf(g) / 3) >= 80 && (Integer.valueOf(g) / 3) <= 100) {
			return 'A';
		} else if ((Integer.valueOf(g) / 3) >= 50 && (Integer.valueOf(g) / 3) < 80) {
			return 'B';
		} else if ((Integer.valueOf(g) / 3) < 50 && (Integer.valueOf(g) / 3) >= 0) {
			return 'C';
		} else
			return ' ';

	}

	@GetMapping("/Grade-json")
	@ResponseBody
	@ExceptionHandler({ExceptionResponse.class})
	public scorecard displayscard(@RequestParam(name = "Physics", required = false, defaultValue = "70") String Physics,
			@RequestParam(name = "Chemistry", required = false, defaultValue = "85") String Chemistry,
			@RequestParam(name = "Math", required = false, defaultValue = "60") String Math) {
		
		scorecard scard = new scorecard();
		jsonErrorResponse jxrsp = new jsonErrorResponse();

		try {
			scard.setPhysics(Physics);
			scard.setChemistry(Chemistry);
			scard.setMath(Math);
			scard.setTotal(total(Physics, Chemistry, Math));
			scard.setGrade(gd(scard.getTotal()));
		} catch (Exception e) {
			jxrsp.setChemistry(null);
			System.gc();
			jxrsp.setPhysics(null);
			System.gc();
			jxrsp.setMath(null);
			System.gc();
			jxrsp.setGrade('0');
			jxrsp.setTotal(0);
			jxrsp.setMessage("NumberFormatException");
			jxrsp.setDetails(""+e);
			//System.out.println("Exception Here : " + e);
			System.gc();
			
		   return jxrsp;
		}

		return scard;
	}
	

	@PostMapping(value = "/Grade-json", consumes = "application/json", produces = "application/json")
	@ResponseBody
	public scorecard acceptscard(@RequestBody scorecard pscard) {
		try {

		} catch (Exception ex) {
			System.out.println("Exception Here : " + ex);
		}

		return pscard;

	}

	@GetMapping("/Grade")
	@ResponseBody
	@ExceptionHandler({ ExceptionResponse.class })
	public ModelAndView htmlView0(Model model,
			@RequestParam(name = "Physics", required = false, defaultValue = "70") String Physics,
			@RequestParam(name = "Chemistry", required = false, defaultValue = "85") String Chemistry,
			@RequestParam(name = "Math", required = false, defaultValue = "60") String Math) {
		scorecard scard = new scorecard();
		ExceptionResponse exrsp = new ExceptionResponse();

		try {

			scard.setPhysics(Physics);
			scard.setChemistry(Chemistry);
			scard.setMath(Math);
			scard.setTotal(total(Physics, Chemistry, Math));
			scard.setGrade(gd(scard.getTotal()));
		} catch (Exception ex) {
			ModelAndView mav0 = new ModelAndView();
			exrsp.setExceptionMsg(" " + ex);
			mav0.addObject("exrsp", exrsp);
			mav0.setViewName("springException");
			//System.out.println("Exception Here : " + ex);
			return mav0;
		}

		ModelAndView mav = new ModelAndView();
		mav.addObject("scard", scard);
		mav.setViewName("scorecard");

		return mav;

	}

	@PostMapping(value = "/Grade", consumes = "application/html", produces = "application/html")
	@ResponseBody
	public ModelAndView htmlView1(Model model) {
		scorecard scard = new scorecard();

		ModelAndView mav = new ModelAndView();
		mav.addObject("scard", scard);
		mav.setViewName("scorecard");
		return mav;

	}

}
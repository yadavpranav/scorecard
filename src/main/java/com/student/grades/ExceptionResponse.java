package com.student.grades;

/******
 * 
 * @author PRANAV YADAV
 *
 */

public class ExceptionResponse extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String exceptionMsg;

	public String getExceptionMsg() {
		return exceptionMsg;
	}

	public void setExceptionMsg(String exceptionMsg) {
		this.exceptionMsg = exceptionMsg;
	}

}